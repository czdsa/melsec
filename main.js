const { app, BrowserWindow } = require("electron");
const { Notification } = require("electron");

function showNotification() {
  const notification = {
    title: "w10通知消息",
    body: "程序正在运行",
  };
  new Notification(notification).show();
}

function createWindow() {
  const win = new BrowserWindow({
    width: 1000,
    height: 900,
    webPreferences: {
      nodeIntegration: true, //允许新开窗口支持Node环境
      enableRemoteModule: true,
      nativeWindowOpen: true,
      contextIsolation: false,
    },
  });

  win.loadFile("pages/index.html");
}

app.whenReady().then(createWindow).then(showNotification);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
