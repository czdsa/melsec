const mc = require("./melsec_1E.js");
const conn1 = new mc({
  host: "192.168.30.1",
  port: 6200,
  IOBase: 8,
  logLevel: -1
});

conn1.connect()
  .then(function () {
    conn1.read('X-20')
      .then(function (X20) {
        console.log(X20)
        //可以添加条件判定
        return conn1.read('X-21')
      })
      .then(function (X21) {
        console.log(X21)
        //操作完毕，断开连接后退出
        conn1.close()
          .then(_ => process.exit())
      })
      .catch(_ => {
        console.log('数据读取失败');
        process.exit()
      }
      )
  })
  .catch(_ => console.log('連接失敗'))

