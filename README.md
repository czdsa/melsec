# 三零PLC网络Socket通讯，支持1E跟3E格式，目前已测试 FX5UC

#### 项目构成

melsec_1E.js(可用), melsec_3E.js(未写完)

#### 安装教程

1.  确保安装有 NODE.js    
2.  npm i    
3.  去到pages目录，尝试运行node test(请自行修改好PLC配置跟组态，参照src目录下两plc图片，关键是IP,二进制通信，SLMP,TCP,端口号)      
4.  目前已写完并测试过FX5U跟Q系列的1E通讯部分代码，3E部分没写玩，有空再补全，自己使用请仔细阅读官方文档跟测试代码           
5.  以下为可选项      安装开发ELECTRON 需要的环境,npm run dev ,网线连到PLC网络，VIEW菜单打开控制台，编写自己的页面逻辑       
6.  修改对应 JS 文件配置与自身环境或 SQL 设置匹配,有需求自己改写     

#### 使用说明

项目目录下 npm run dev,Node 会从 main.js 开始加载,在浏览器（支持 NODE 环境）打开 pages/index.html    

```
    const mc = require("./melsec_1E.js");
    const conn1 = new mc({
      host: "192.168.30.1", // PLC的IP,部分报文的网络设置更改有待测试
      port: 6200, // 在PLC设置里开放6200的SOCKET端口，通讯格式选二进制
      IOBase:8,  //通常三菱输入X输出Y点编码为8进制或16进制,其他默认10进制(16进制需转换10进制)
      logLevel: -1 //无提示,-1至2,值越大控制台消息越详细
      //conn1.write().then(成功).catch(失败)
      //conn1.read().then(返回一个读取到的值的数组,Bit为0/1,WORD类型为16进制,需自己转换需要的类型).catch(失败)
    });
    conn1.connect();

conn1.write('D-100',1234).then(console.log).catch(_=>console.log('err'))
conn1.read('M-10').then(console.log).catch(_=>console.log('err'))
read:  read('M-100') read('M-100',10) //M100-109  read('D-100')  read('D-100',2) //D100，D101
write: write('M-100',1) write('M-100',[1,0,1,1]) //写入M100 为 ON, M101 为 OFF, ...
write('D-100',1234) write('D-100',['1234','FFFF','0015','12'])//写入D100 为 16#1234 (16进制16位无符号)
write(['M-10','M-40','Y-17','Y-20'],[1,0,1,1])//M10 = 1, M40 = 0, ...
write(['D-100','TN-20','D-120','D-121'],['1234','FF','0015','12'])//D100 =1234 ,T20的当前值 = FF, ... 
支持的PLC寄存器类型X Y M S D R TS(触点) TN(计时器当前值) CS CN
```
#### 里面有关于怎么解析读取的16进制为其他格式,需要注意三菱等日系PLC的高低位与西门子等欧美系相反   
#### 这里测试以FX5U为准，输入X输出Y是8进制编码，其他默认10进制编码，程序转换为内部16进制地址, 部分PLC输入输出是16进制编码   
