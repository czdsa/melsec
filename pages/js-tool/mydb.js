var mysql = require('mysql');

const config = {
  connectionLimit: 10,
  host: '',
  user: 'mysql',
  password: '',
  database: 'test',
  connectTimeout: 50000
  // port: '10088',
  // socketPath: '/tmp/mysql.sock'
}

var pool = mysql.createPool(config)

pool.getConnection(function (err, connection) {
  if (err) throw err; // not connected!

  // Use the connection
  connection.query('select * from `dbo.plc`',
    // "CREATE TABLE `dbo.plc` ( `id` INT(50) NOT NULL AUTO_INCREMENT , `name` VARCHAR(50) NOT NULL , `c_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP , `info` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`))"
    function (error, results, fields) {
      // When done with the connection, release it.
      console.log(results);
      connection.release();

      pool.end(function (err) {
        // all connections in the pool have ended
      });

      // Handle error after the release.
      if (error) throw error;

      // Don't use the connection here, it has been returned to the pool.
    });
});

